/**
 * Copyright 2015 Robert Schroll
 * Copyright 2022 Emanuele Sorce
 *
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL v3. See the file COPYING for full details.
 */

#ifndef PDFREADER_H
#define PDFREADER_H

#include <QObject>
#include <QVariant>
#include <QFuture>
#include <QVector>
#include <poppler/qt5/poppler-qt5.h>
#include "../qhttpserver/qhttpresponse.h"
#include <QMutex>

class PDFReader : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString hash READ hash)
    Q_PROPERTY(QString title READ title)
    Q_PROPERTY(int width READ width WRITE setWidth)
    Q_PROPERTY(int height READ height WRITE setHeight)
public:
    explicit PDFReader(QObject *parent = 0);
    ~PDFReader();
    QString hash();
    QString title();
    int width();
    void setWidth(int value);
    int height();
    void setHeight(int value);

    Q_INVOKABLE bool load(const QString &filename);
    // load() will make sure to unload the second time is called, so unload is not mandatory
    void unload();
    Q_INVOKABLE void serveComponent(int pagenum, QHttpResponse *response);
    Q_INVOKABLE int numberOfPages();
    Q_INVOKABLE QVariantMap getCoverInfo(int thumbsize, int fullsize);
    Q_INVOKABLE QVariantList getContents();

signals:
    void contentsReady(QVariantList contents);

private:
    QMutex rendering_count_mutex;
    int rendering_count;
    void renderingCountIncrease();
    void renderingCountDecrease();

    // use mutex to avoid race condition in rendering pages
    QMutex cache_mutex;

    // The page cache is a vector as big as the number of pages
    // list of the images loaded
    QVector<int> pagecache_ready;
    // the rendered pages
    QVector<QByteArray> pagecache;

    // pages that we are rendering now
    QVector<int> pages_rendering;

    void finishServeComponent(QHttpResponse *response, QFutureWatcher<QByteArray> *watcher, QFuture<QByteArray> *page, int pagenum);
    bool parse();
    QVariantList parseContents(QDomElement el);
    void computeHash(const QString &filename);
    void readMetadata();
    QByteArray renderPage(int pageNum, QString hash);

    Poppler::Document *pdf;
    QString _hash;
    QStringList spine;
    QVariantMap metadata;
    int _width;
    int _height;
};

#endif // PDFREADER_H
