/* Copyright 2013-2015 Robert Schroll
 * Copyright 2018-2020 Emanuele Sorce
 *
 * This file is part of Beru and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 *
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.LocalStorage 2.0
import QtQuick.Layouts 1.3

Page {
	id: localBooks

	property alias sort: sorttabs.currentIndex
	property bool needsort: false
	property bool wide: width > scaling.dp(800)
	property string bookdir: ""
	property bool readablehome: false
	property string defaultdirname: "Books"
	property double gridmargin: scaling.dp(10)
	property double mingridwidth: scaling.dp(150)
	property bool authorinside: false

    onSortChanged: {
		authorinside = false;
    }

	// when closing perAuthor page reset model
	onAuthorinsideChanged: {
		if(!authorinside)
			perAuthorModel.clear();
	}

    header: Column {
		width: parent.width
		ToolBar {
			width: parent.width
			RowLayout {
				spacing: scaling.dp(2)
				anchors.top: parent.top
				anchors.right: parent.right
				anchors.bottom: parent.bottom
				width: parent.width - scaling.dp(10)

				Label {
					text: gettext.tr("Library")
					font.pixelSize: headerTextSize
					elide: Label.ElideRight
					horizontalAlignment: Qt.AlignLeft
					verticalAlignment: Qt.AlignVCenter
					Layout.fillWidth: true
				}

				ToolbarIcon {
					name: "add"
					onClicked: importer.pick()
				}

				ToolbarIcon {
					name: "info"
					onClicked: pageStack.push("About.qml")
				}

				ToolbarIcon {
					name: "settings"
					onClicked: pageStack.push("Settings.qml")
				}
			}
		}
		TabBar {
			id: sorttabs
			width: parent.width
			TabButton {
				text: gettext.tr("Recently Read")
			}
			TabButton {
				text: gettext.tr("Title")
			}
			TabButton {
				text: gettext.tr("Author")
			}
		}
	}

	footer: ToolBar {
		visible: coverTimer.running || refreshTimer.running

		anchors.bottom: parent.bottom

		RowLayout {
			spacing: scaling.dp(20)

			BusyIndicator {
				id: updatingIndicator
				height: scaling.dp(45)
				width: height
			}

			Label {
				id: updatingLabel
				text: refreshTimer.running ? gettext.tr("Loading library...") : gettext.tr("Processing books data...");
				elide: Text.ElideRight
				verticalAlignment: Text.AlignVCenter
				Layout.fillWidth: true
			}
		}
	}

    function fileToTitle(filename) {
        return filename.replace(/\.\w+$/, "").replace(/_/g, " ")
    }

    function listBooks() {

		gridModel.update();
		titleModel.update();
		authorModel.update();

		localBooks.needsort = false;
    }

    function listAuthorBooks(authorsort) {
		perAuthorModel.update(authorsort);
		authorinside = true;
    }

    function addFile(filePath, startCoverTimer) {
		book_db.addFile(filePath, fileToTitle(filePath.split("/").pop()));

        localBooks.needsort = true
        if (startCoverTimer)
            coverTimer.start()
    }

    function addBookDir() {
		book_db.addBookDir(bookdir, (path) => fileToTitle(path.split("/").pop()) );
        localBooks.needsort = true
    }

    function updateRead(filename) {
		book_db.updateRead(filename)
        if (localBooks.sort == 0)
            listBooks()
    }

    // When there are new books in the library, this function takes the first of those books and extracts its data
    // It uses a timer to read books gradually, one at a time.
    function updateBookCover() {
        var db = book_db.getHandle()
        db.transaction(function (tx) {
            var res = tx.executeSql("SELECT filename, title FROM LocalBooks WHERE authorsort == 'zzznull'")
			if (res.rows.length == 0) {
				// there are no more books unordered
				// stop the loop - and refresh authors
				authorModel.update();
				return
			}

            localBooks.needsort = true

            var title = res.rows.item(0).title
            var author = gettext.tr("Could not open this book.")
            var authorsort = "zzzzerror"
            var cover = "ZZZerror"
            var fullcover = ""
            var hash = ""
			var description = ""
			var subject = ""
			var language = ""

			if (coverReader.load(res.rows.item(0).filename)) {
                var coverinfo = coverReader.getCoverInfo(Math.max(scaling.dp(64), mingridwidth), 2*mingridwidth)
                title = coverinfo.title
                if (title == "ZZZnone")
                    title = res.rows.item(0).title

                author = coverinfo.author.trim()
                authorsort = coverinfo.authorsort.trim()
				if(typeof(coverinfo.description) == "string") description = coverinfo.description.trim()
				if(typeof(coverinfo.subject) == "string") subject = coverinfo.subject.trim()
				if(typeof(coverinfo.language) == "string") language = coverinfo.language.trim()

                if (authorsort == "zzznone" && author != "") {
                    // No sort information, so let's do our best to fix it:
                    authorsort = author
                    var lc = author.lastIndexOf(",")
                    if (lc == -1) {
                        // If no commas, assume "First Last"
                        var ls = author.lastIndexOf(" ")
                        if (ls > -1) {
                            authorsort = author.slice(ls + 1) + ", " + author.slice(0, ls)
                            authorsort = authorsort.trim()
                        }
                    } else if (author.indexOf(",") == lc) {
                        // If there is exactly one comma in the author, assume "Last, First".
                        // Thus, authorsort is correct and we have to fix author.
                        author = author.slice(lc + 1).trim() + " " + author.slice(0, lc).trim()
                    }
                }

                cover = coverinfo.cover
                fullcover = coverinfo.fullcover
                hash = coverReader.hash()
            }

            tx.executeSql("UPDATE LocalBooks SET title=?, author=?, authorsort=?, cover=?, " +
                          "fullcover=?, hash=?, description=?, subject=?, language=? WHERE filename=?",
                          [title, author, authorsort, cover, fullcover, hash, description, subject, language, res.rows.item(0).filename])

			// Search the book on all the views and update its data
            if (localBooks.visible) {
				function populateBookObj(book) {
					if (book.filename == res.rows.item(0).filename) {
						book.title = title
                        book.author = author
						book.description = description
						book.subject = subject
						book.language = language
                        book.cover = cover
                        book.fullcover = fullcover
						return true;
					}
					return false;
				}
				for (var i=0; i<gridModel.count; i++) {
					if(populateBookObj(gridModel.get(i))) break;
                }
                for (var i=0; i<titleModel.count; i++) {
					if(populateBookObj(titleModel.get(i))) break;
                }
                for (var i=0; i<authorModel.count; i++) {
					if(populateBookObj(authorModel.get(i))) break;
                }
            }

            coverTimer.start()
        })
    }

    function openInfoDialog(book) {

		/*
        var dirs = ["/.local/share/%1", "/.local/share/ubuntu-download-manager/%1"]
        for (var i=0; i<dirs.length; i++) {
            var path = filesystem.homePath() + dirs[i].arg(Qt.application.name)
            if (book.filename.slice(0, path.length) == path) {
                allowDelete = true
                break
            }
        }*/
		pageStack.push("BookInfo.qml", {
			bookdata: new Object(book)
		})
    }

    function refreshCover(filename) {
		book_db.refreshCover(filename)
        coverTimer.start()
    }

    function inDatabase(hash, existsCallback, newCallback) {
        book_db.inDatabase(hash, existsCallback, newCallback)
    }

    function readBookDir() {
        addBookDir()
        listBooks()
        coverTimer.start()
    }

    function loadBookDir() {
        if (readablehome) {
            readablehome = true
            var storeddir = getSetting("bookdir")
            bookdir = (storeddir == null) ? filesystem.getDataDir(defaultdirname) : storeddir
        } else {
            readablehome = false
            bookdir = filesystem.getDataDir(defaultdirname)
        }
    }

    function setBookDir(dir) {
        bookdir = dir
        setSetting("bookdir", dir)
    }

    // We need to wait for main to be finished, so that the settings are available.
	function onMainCompleted() {
		refreshTimer.start();
	}

    Timer {
		id: refreshTimer
		repeat: false
		interval: 1
		running: false
		triggeredOnStart: false

		onTriggered: {
			// readBookDir() will trigger the loading of all files in the default directory
			// into the library.
			if (!book_db.first_start) {
				loadBookDir()
			} else {
                //readablehome = filesystem.readableHome()
                //if (readablehome) {
                //	setBookDir(filesystem.homePath() + "/" + defaultdirname)
                //	settingsDialog.open()
                //} else {
					setBookDir(filesystem.getDataDir(defaultdirname))
                //}
			}
			readBookDir()
		}
	}

    // If we need to resort, do it when hiding or showing this page
    onVisibleChanged: {
        if (needsort)
            listBooks()
        // If we are viewing recently read, then the book we had been reading is now at the top
        if (visible && sort == 0)
            gridview.positionViewAtBeginning()
    }

    Reader {
        id: coverReader
    }

    Timer {
        id: coverTimer
        interval: 1000
        repeat: false
        running: false
        triggeredOnStart: false

        onTriggered: localBooks.updateBookCover()
    }

	function prepareBookData(book) {
		book.fullcover = book.fullcover || "ZZZnull";
		book.count = book["count(*)"]
		return book;
	}

	// contains the model for the grid view (recent books)
	ListModel {
		id: gridModel

		function update() {
			clear();

			var res = book_db.getBooksByLastread()

			for (var i=0; i<res.rows.length; i++) {
				var item = prepareBookData(res.rows.item(i))
				if (filesystem.exists(item.filename))
					append(item)
			}
		}
	}

	// contains the model for the title view
	ListModel {
		id: titleModel

		function update() {
			clear();

			var res = book_db.getBooksByTitle()
			for (var i=0; i<res.rows.length; i++) {
				var item = prepareBookData(res.rows.item(i))
				if (filesystem.exists(item.filename))
					append(item)
			}
		}
	}

	// contains the model for the author view
	ListModel {
		id: authorModel

		function update() {
			clear();

			var res = book_db.getBooksByAuthor()
			for (var i=0; i<res.rows.length; i++) {
				var item = prepareBookData(res.rows.item(i))
				if (filesystem.exists(item.filename))
					append(item)
			}
		}
	}

	// contains the model for the single author books view
    ListModel {
        id: perAuthorModel

        function update(authorsort) {
			clear()

			var res = book_db.getAuthorSort(authorsort)

			for (var i=0; i<res.rows.length; i++) {
				var item = prepareBookData(res.rows.item(i))
				if (filesystem.exists(item.filename))
					append(item)
			}
			append({filename: "ZZZback", title: gettext.tr("Back"), author: "", cover: ""})
		}
    }

    SwipeView {
		id: swiper

		anchors.fill: parent

		currentIndex: localBooks.sort
		onCurrentIndexChanged: {
			localBooks.sort = currentIndex
		}

		Item {
			GridView {
				id: gridview

				anchors.fill: parent
				anchors.leftMargin: gridmargin
				anchors.rightMargin: gridmargin

				clip: true
				cellWidth: width / Math.floor(width/mingridwidth)
				cellHeight: cellWidth*1.5

				model: gridModel
				delegate: CoverDelegate {
					width: gridview.cellWidth
					height: gridview.cellHeight

					cover: model.cover
					fullcover: model.fullcover
					title: model.title
					author: model.author
					coverMargin: gridmargin

					MouseArea {
						anchors.fill: parent
						onClicked: {
							// Save copies now, since these get cleared by loadFile (somehow...)
							var filename = model.filename
							var pasterror = model.cover == "ZZZerror"
							if (loadFile(filename) && pasterror)
								refreshCover(filename)
						}
						onPressAndHold: {
							openInfoDialog(model);
						}
					}
				}

				ScrollBar.vertical: ScrollBar { }
			}
		}


		ListView {
			id: titleview

			clip: true

			delegate: SuperDelegate {
				width: titleview.width
				image_source: model.filename == "ZZZback" ? "Icons/go-previous.svg" :
					model.cover == "ZZZnone" ? defaultCover.missingCover(model) :
					model.cover == "ZZZerror" ? "images/error_cover.svg" : model.cover
				main_text: model.title
				sub_text: model.author
				onClicked: {
					if (model.filename == "ZZZback") {
						authorinside = false;
					} else {
						// Save copies now, since these get cleared by loadFile (somehow...)
						var filename = model.filename
						var pasterror = model.cover == "ZZZerror"
						if (loadFile(filename) && pasterror)
							refreshCover(filename)
					}
				}
				onPressAndHold: {
					if (model.filename != "ZZZback")
						openInfoDialog(model)
				}
			}
			model: titleModel

			ScrollBar.vertical: ScrollBar { }
		}

		Item {
			ListView {
				id: authorview

				visible: wide || !authorinside
				anchors.left: parent.left
				height: parent.height
				width: wide ? Math.floor(parent.width * 0.5) : parent.width

				clip: true

				model: authorModel
				delegate: SuperDelegate {
					width: authorview.width
					main_text: model.author || gettext.tr("Unknown Author")
					sub_text: (model.count > 1) ? gettext.tr("%1 Book", "%1 Books", model.count).arg(model.count) : model.title
					image_source: model.count > 1 ? "Icons/avatar.svg" :
						model.filename == "ZZZback" ? "Icons/go-previous.svg" :
						model.cover == "ZZZnone" ? defaultCover.missingCover(model) :
						model.cover == "ZZZerror" ? "images/error_cover.svg" :
						model.cover
					onClicked: {
						if (model.count > 1) {
							listAuthorBooks(model.authorsort)
							//adjustViews(true)
						} else {
							// Save copies now, since these get cleared by loadFile (somehow...)
							var filename = model.filename
							var pasterror = model.cover == "ZZZerror"
							if (loadFile(filename) && pasterror)
								refreshCover(filename)
						}
					}
					onPressAndHold: {
						if (model.count == 1)
							openInfoDialog(model)
					}
				}

				ScrollBar.vertical: ScrollBar { }
			}

			ListView {
				id: perAuthorListView
				height: parent.height
				anchors.right: parent.right
				width: wide ? Math.floor(parent.width * 0.5) : parent.width
				visible: wide || !authorview.visible
				clip: true

				model: perAuthorModel
				delegate: SuperDelegate {
					width: perAuthorListView.width
					visible: model.filename != "ZZZback" || !wide
					image_source: model.filename == "ZZZback" ? "Icons/go-previous.svg" :
						model.cover == "ZZZnone" ? defaultCover.missingCover(model) :
						model.cover == "ZZZerror" ? "images/error_cover.svg" : model.cover
					main_text: model.title
					sub_text: model.author
					onClicked: {
						if (model.filename == "ZZZback") {
							authorinside = false;
						} else {
							// Save copies now, since these get cleared by loadFile (somehow...)
							var filename = model.filename
							var pasterror = model.cover == "ZZZerror"
							if (loadFile(filename) && pasterror)
								refreshCover(filename)
						}
					}
					onPressAndHold: {
						if (model.filename != "ZZZback")
							openInfoDialog(model)
					}
				}

				ScrollBar.vertical: ScrollBar { }
			}
		}
	}
	// Bottom page indicator for the swiper
	PageIndicator {
		id: indicator

		count: swiper.count
		currentIndex: swiper.currentIndex

		anchors.bottom: swiper.bottom
		anchors.horizontalCenter: swiper.horizontalCenter
	}

    Item {
        anchors.fill: parent
        visible: gridModel.count == 0 && !refreshTimer.running

        Column {
            anchors.centerIn: parent
            spacing: scaling.dp(16)
            width: Math.min(scaling.dp(600), parent.width - scaling.dp(8))

            Label {
                id: noBooksLabel
				anchors.horizontalCenter: parent.horizontalCenter
                text: book_db.first_start ? gettext.tr("Welcome to Sturm Reader!") : gettext.tr("No Books in Library")
				font.pixelSize: scaling.dp(25)
				horizontalAlignment: Text.AlignHCenter
				width: parent.width
				wrapMode: Text.Wrap
            }

            Label {
                /*/ A path on the file system. /*/
                text: gettext.tr("Sturm Reader could not find any books for your library, and will " +
                              "automatically find all epub files in <i>%1</i>.  Additionally, any book " +
                              "opened will be added to the library.").arg(bookdir)
                wrapMode: Text.Wrap
				anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
            }

            Button {
                text: gettext.tr("Get Books")
				anchors.horizontalCenter: parent.horizontalCenter
                highlighted: true
                width: parent.width
                onClicked: importer.pick()
            }

            Button {
                text: gettext.tr("Search Again")
				anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                onClicked: refreshTimer.start();
            }
        }
    }
}
