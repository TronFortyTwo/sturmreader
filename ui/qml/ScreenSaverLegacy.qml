/* Copyright 2024 Emanuele Sorce
 * 
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */


import QtQuick 2.12
import QtSystemInfo 5.0

// Keep screen on
ScreenSaver {
    property bool enabled: false
    
    screenSaverEnabled: enabled
}