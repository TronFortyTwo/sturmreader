/*
 * Copyright 2022 Emanuele Sorce
 *
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPLv3. See the file COPYING for full details.
 */

 // Unified database management

import QtQuick 2.12
import QtQuick.LocalStorage 2.0

Item {
    // New items are given a lastread time of now, since these are probably
    // interesting for a user to see.
    property string addFileSQL: "INSERT OR IGNORE INTO LocalBooks(filename, title, author, authorsort, " +
                                "cover, lastread) VALUES(?, ?, '', 'zzznull', 'ZZZnone', datetime('now'))"

    property string fields: "filename, title, author, cover, fullcover, authorsort, subject, language, description"

    property bool first_start: false


    function open() {
        return LocalStorage.openDatabaseSync("BeruLocalBooks", "", "Books on the local device", 1000000, (db) => {
            db.changeVersion(db.version, "1")
            first_start = true;
        });
    }

    function removeFile(filePath) {
        var db = open()

        db.transaction((tx) => {
            tx.executeSql("DELETE FROM LocalBooks WHERE filename=?", [filePath])
        });
    }

	function addFile(filePath, title) {
        var db = open()
        db.transaction(function (tx) {
            tx.executeSql(addFileSQL, [filePath, title])
        })
	}

	function addBookDir(bookdir, titleCb) {
		var db = open()
        db.transaction(function (tx) {
            var files = filesystem.listDir(bookdir, ["*.epub", "*.cbz", "*.pdf", "*.cbr"])
            for (var i=0; i<files.length; i++) {
                tx.executeSql(addFileSQL, [files[i], titleCb(files[i])])
            }
        })
	}

	function updateRead(filename) {
        var db = open()
        db.transaction(function (tx) {
            tx.executeSql("UPDATE OR IGNORE LocalBooks SET lastread=datetime('now') WHERE filename=?",
                          [filename])
        })
	}

	function inDatabase(hash, existsCallback, newCallback) {
        var db = open()
        db.readTransaction(function (tx) {
            var res = tx.executeSql("SELECT filename FROM LocalBooks WHERE hash == ?", [hash])
            if (res.rows.length > 0 && filesystem.exists(res.rows.item(0).filename))
                existsCallback(res.rows.item(0).filename)
            else
                newCallback()
        })
    }

	function refreshCover(filename) {
        var db = open()
        db.transaction(function (tx) {
            tx.executeSql("UPDATE LocalBooks SET authorsort='zzznull' WHERE filename=?", [filename])
        })
	}

	function getBooksByLastread() {
		var db = open()
        var res
		db.readTransaction(function (tx) {
			res = tx.executeSql("SELECT " + fields + ", count(*) FROM LocalBooks GROUP BY filename ORDER BY lastread DESC, title ASC")
		})
		return res
	}

	function getBooksByTitle() {
		var db = open()
        var res
		db.readTransaction(function (tx) {
			res = tx.executeSql("SELECT " + fields + ", count(*) FROM LocalBooks GROUP BY filename ORDER BY title ASC")
		})
		return res
	}

	function getBooksByAuthor() {
		var db = open()
        var res
		db.readTransaction(function (tx) {
			res = tx.executeSql("SELECT " + fields + ", count(*) FROM LocalBooks GROUP BY authorsort ORDER BY authorsort ASC")
		})
		return res
	}

	function getAuthorSort(authorsort) {
		var db = open()
        var res
		db.readTransaction(function (tx) {
			res = tx.executeSql("SELECT " + fields + " FROM LocalBooks WHERE authorsort=? ORDER BY title ASC", [authorsort])
		})
		return res
	}

	function getHandle() {
		return open()
	}

    function triggerRefresh(){
        var db = open()

        db.transaction((tx) => {
            tx.executeSql("UPDATE LocalBooks SET authorsort='zzznull'")
        })
    }

	Component.onCompleted: {
        var db = open();
        var trigger_refresh = false;

        db.transaction(function (tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS LocalBooks(filename TEXT UNIQUE, " +
                          "title TEXT, author TEXT, cover BLOB, lastread TEXT)")
        })
        // NOTE: db.version is not updated live!  We will get the change only the next time
        // we run, so here we must keep track of what's been happening.  onFirstStart() has
        // already run, so we're at version 1, even if db.version is empty.
        if (db.version == "" || db.version == "1") {
            db.changeVersion(db.version, "2", function (tx) {
                tx.executeSql("ALTER TABLE LocalBooks ADD authorsort TEXT NOT NULL DEFAULT 'zzznull'")
            })
            trigger_refresh = true
        }
        if (db.version == "" || db.version == "1" || db.version == "2") {
            db.changeVersion(db.version, "3", function (tx) {
                tx.executeSql("ALTER TABLE LocalBooks ADD fullcover BLOB DEFAULT ''")
            })
            trigger_refresh = true
        }
        if (db.version == "" || db.version == "1" || db.version == "2" || db.version == "3") {
            db.changeVersion(db.version, "4", function (tx) {
                tx.executeSql("ALTER TABLE LocalBooks ADD hash TEXT DEFAULT ''")
            })
            trigger_refresh = true
        }
        if (db.version == "" || db.version == "1" || db.version == "2" || db.version == "3" || db.version == "4") {
            db.changeVersion(db.version, "5", function (tx) {
                tx.executeSql("ALTER TABLE LocalBooks ADD description TEXT DEFAULT ''")
                tx.executeSql("ALTER TABLE LocalBooks ADD subject TEXT DEFAULT ''")
                tx.executeSql("ALTER TABLE LocalBooks ADD language TEXT DEFAULT ''")
            })
            trigger_refresh = true
        }

        if(trigger_refresh) triggerRefresh();
    }
}